FROM ubuntu:22.04

ARG VERSION="1.4.13"
ARG DEBIAN_FRONTEND=noninteractive

LABEL maintainer="dominik.halley@googlemail.com"
LABEL version="v${VERSION}"
LABEL descripton="Repetier Server docker container"

RUN apt update
RUN apt upgrade -y
RUN apt autoremove

ADD https://download3.repetier.com/files/server/debian-amd64/Repetier-Server-${VERSION}-Linux.deb repetier-server.deb

RUN dpkg --unpack repetier-server.deb  && rm -rf repetier-server.deb
RUN mkdir -p /data && sed -i "s/var\/lib\/Repetier-Server/data/g" /usr/local/Repetier-Server/etc/RepetierServer.xml

EXPOSE 3344

CMD [ "/usr/local/Repetier-Server/bin/RepetierServer", "-c", "/usr/local/Repetier-Server/etc/RepetierServer.xml" ]
