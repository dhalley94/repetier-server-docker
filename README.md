# Repetier-Server Docker

## Information

I created a simple docker container for repetier-server free version. Container is in production use right now.

## Gettings started

1. Clone this repository
    ``` git clone https://gitlab.com/dhalley94/repetier-server-docker.git ```
2. Modify ```docker-compose.yaml``` file to fit your needs
3. Execute run.sh
    ``` sh run.sh ```
